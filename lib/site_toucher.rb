module SiteToucher
  def self.extended(base)
    base.send :include, InstanceMethods
  end

  def full_rebase_timestamp(*hooks)
    hooks.each { |h| self.send(h, :site_touch_modified_at) }
  end

  module InstanceMethods
    def site_touch_modified_at
      self.site.touch_modified_at
    end
  end
end
