module Urler
  def locale_path
    if self.new_record?
      UrlGenerator.new.send("site_#{self.class.to_s.downcase.pluralize}_path", @locale, self.site)
    else
      UrlGenerator.new.send("site_#{self.class.to_s.downcase}_path", @locale, self.site, self.id)
    end
  end
end
