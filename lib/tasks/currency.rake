namespace :currency do
  MapR = lambda { |x| x.split('.').values_at(2,1,0) }

  desc "3Banka: dollar update"
  task :update => :environment do
    make_api_request("current_date", Date.today.strftime("%d.%m.%Y"))
    ['usd', 'eur'].each do |currency|
      currency_data = get_currency_data(currency, Date.today-1.month, Date.today+1.day)
      make_api_request(currency, currency_data[:date_map][Date.today.strftime("%d.%m.%Y")][:value])
      make_api_request("#{currency}_series", currency_data[:content])
      make_api_request("#{currency}_table", currency_data[:table])
    end

    ['usd', 'eur'].each do |currency|
      currency_data = get_currency_data(currency, Date.today-12.months, Date.today+1.day)
      make_api_request("#{currency}_series_year", currency_data[:content])
      make_api_request("#{currency}_table_year", currency_data[:table])
    end
  end

  def make_api_request(template_name, content)
    uri = URI("#{Simplepage.config.api_host}/api/sites/#{Simplepage.config.api_site_id}/templates/update")
    res = Net::HTTP.post_form(
      uri,
      'api_key' => Simplepage.config.api_key,
      'name'    => template_name,
      'content' => content,
      'sign'    => make_api_sign(template_name, content)
    )
  end

  def make_api_sign(template_name, content)
    Digest::MD5.hexdigest([Simplepage.config.api_email, Simplepage.config.api_pass, Simplepage.config.api_site_id, template_name, content].join(''))
  end

  def get_currency_data(currency, start_date, end_date)
    data = CBR.dynamic(currency, start_date, end_date)
    if data.records
      date_map = {}
      if data.records.length > 0
        data.records.each do |record|
          date_map[record.date.strftime("%d.%m.%Y")] = { value: record.value }
        end

        prev_value = date_map[data.records[0].date.strftime("%d.%m.%Y")][:value]

        (data.records[1].date .. Date.today).each do |date|
          puts "loop: #{date.to_s}"
          if date_map[date.strftime("%d.%m.%Y")]
            prev_value = date_map[date.strftime("%d.%m.%Y")][:value]
          else
            date_map[date.strftime("%d.%m.%Y")] = { value: prev_value }
          end
        end

        chart_labels = []
        chart_data = []

        date_map.keys.sort_by(&MapR).each do |key|
          chart_labels.push(key)
          chart_data.push(date_map[key][:value])
        end
        puts chart_labels
        puts chart_data

        content = "chart_labels = ['#{chart_labels.join("','")}'];\n"
        content += "chart_data = [#{chart_data.join(',')}];\n"
        puts content
        table = "<table>"
        chart_labels.reverse.each_with_index do |cl, index|
          table += "<tr><td>#{cl}</td><td>#{chart_data[-(index+1)]}</td></tr>"
        end
        table += "</table>"
        puts table
        {
          date_map: date_map,
          chart_labels: chart_labels,
          chart_data: chart_data,
          content: content,
          table: table
        }
      end
    end
  end
end
