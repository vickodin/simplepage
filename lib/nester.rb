module Nester
  def parents
    if p = self.parent
      return [self, p.parents].flatten
    else
      return [self]
    end
  end
end
