module TitledScope
  def self.extended(base)
    base.class_eval do
      scope :titled, -> { select('id, name, site_id') }
    end
  end
end
