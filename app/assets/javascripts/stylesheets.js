$(function() {
  var el = document.getElementById('stylesheet_content');
  if (el) {
    var myCodeMirror = CodeMirror.fromTextArea(el, {
      lineNumbers: true,
      mode: 'css',
      theme: 'cobalt',
      tabSize: 2
    });
  }
});

