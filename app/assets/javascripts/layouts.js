$(function() {
  var el = document.getElementById('layout_content');
  if (el) {
    var myCodeMirror = CodeMirror.fromTextArea(el, {
      lineNumbers: true,
      mode: 'htmlmixed',
      theme: 'cobalt'
    });
  }
});
