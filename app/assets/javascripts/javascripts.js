$(function() {
  var el = document.getElementById('javascript_content');
  if (el) {
    var myCodeMirror = CodeMirror.fromTextArea(el, {
      lineNumbers: true,
      mode: 'javascript',
      theme: 'cobalt',
      tabSize: 2
    });
  }
});

