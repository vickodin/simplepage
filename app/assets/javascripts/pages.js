$(function() {
  var el = document.getElementById('page_content');
  if (el) {
    var myCodeMirror = CodeMirror.fromTextArea(el, {
      lineNumbers: true,
      mode: 'htmlmixed',
      theme: 'cobalt',
      extraKeys: {
        "Ctrl-S": function(instance) { saveText(instance.getValue());}
      }
    });
  }
});
