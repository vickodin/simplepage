// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery-fileupload
//= require_tree .

$(function () {
  $('#upload_files input[file]').fileupload({
    sequentialUploads: false,
    singleFileUploads: false,
      dataType: 'script',
      done: function (e, data) {
      },
      add: function (e, data) {
        //data.paramName = "photo[path]"
        data.submit();
      }
  });
});

$(function(){
  $('#login a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });
  $("#forgot_password").click(function(e) {
    e.preventDefault();
    $('#login a:last').tab('show');
  });
  $('#structure a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
    $('.breadcrumb li.active').html($(this).html());
  });
  $('#site a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });
  $("[rel='tooltip']").tooltip({html: true});
  $("[rel='tooltip nofollow']").tooltip();
  $("#more").click(function(e) {
    e.preventDefault();
    $('.more').toggle();
  });
});

function saveText() {
  $('#code_form').submit();
  return false;
}

function checker(site_id) {
  setTimeout('location.reload()', 5000)
}

$(function() {
  var el = document.getElementById('code_content');
  if (el) {
    var myCodeMirror = CodeMirror.fromTextArea(el, {
      lineNumbers: true,
      mode: 'htmlmixed',
      theme: 'xq-light',
      extraKeys: {
        "Ctrl-S": function(instance) { saveText(instance.getValue());},
        "F11": function(cm) {
          setFullScreen(cm, !isFullScreen(cm));
        },
        "Esc": function(cm) {
          if (isFullScreen(cm)) setFullScreen(cm, false);
        }
      }
    });

    function isFullScreen(cm) {
      return /\bCodeMirror-fullscreen\b/.test(cm.getWrapperElement().className);
    }
    function winHeight() {
      return window.innerHeight || (document.documentElement || document.body).clientHeight;
    }
    function setFullScreen(cm, full) {
      var wrap = cm.getWrapperElement();
      if (full) {
        wrap.className += " CodeMirror-fullscreen";
        wrap.style.height = winHeight() + "px";
        document.documentElement.style.overflow = "hidden";
      } else {
        wrap.className = wrap.className.replace(" CodeMirror-fullscreen", "");
        wrap.style.height = "";
        document.documentElement.style.overflow = "";
      }
      cm.refresh();
    }
    CodeMirror.on(window, "resize", function() {
      var showing = document.body.getElementsByClassName("CodeMirror-fullscreen")[0];
      if (!showing) return;
      showing.CodeMirror.getWrapperElement().style.height = winHeight() + "px";
    });
  }
});
