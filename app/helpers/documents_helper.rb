module DocumentsHelper
  def file_tree file
    tree = []
    file.parents.each do |f|
      tree.push(f)
    end
    tree.reverse
  end
end
