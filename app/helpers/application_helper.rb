module ApplicationHelper
  def breadcrumb history, more=nil, more_content=nil
    if history && history.length > 0
      html =  '<ul class="breadcrumb pull-left">'
      last = history.pop
      html += history.map {|item| ['<li>', link_to(item[0], item[1]), '<span class="divider">/</span>', '</li>'].join('')}.join('')
      html += "<li class='active'>#{last}</li>"
      html += '</ul>'

      if more
        unless more_content
          more_content = [
            ["Pages", site_pages_path(@locale, @site)],
            ["CSS", site_stylesheets_path(@locale, @site)],
            ["JS", site_javascripts_path(@locale, @site)],
            ["Templates", site_templates_path(@locale, @site)],
            ["Layouts", site_layouts_path(@locale, @site)],
            ["Images", site_images_path(@locale, @site)],
            ["Files", site_files_path(@locale, @site)]
          ]
        end
        html +=  '<ul class="breadcrumb pull-right">'
        html += more_content.map {|item| ['<li>', link_to(item[0], item[1]), '</li>'].join('')}.join('<span class="divider"> : </span>')
        html += '</ul>'
      end

      html += '<div class="clearfix"></div>'
      return html.html_safe
    end
  end

  def stats count
    if count < 10
      return count
    elsif count < 1000
      return "#{count/10*10}+"
    elsif count < 10000
      return "#{count/1000*1000}+"
    elsif count < 100000
      return "#{count/10000*10000}+"
    elsif count < 1000000
      return "#{count/100000*100000}+"
    else
      return "#{count/1000000*1000000}+"
    end
  end

  def destroy_link_to(path)
    link_to "<i class='icon-remove'></i>".html_safe, path, data: {confirm: 'Are you sure?', toggle: 'tooltip'}, method: :delete, title: "Destroy", rel: 'tooltip', class: 'pad'
  end
end
