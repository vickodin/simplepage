class SiteDestroyJob < Struct.new(:site_id)
  def perform
    site.destroy
  end

  def success(job)
  end

  private
  def site
    Site.find(site_id)
  end
end
