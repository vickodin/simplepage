class SiteCopierJob < Struct.new(:theme_id, :site_id)
  def perform
    c = SiteCopier.new(theme)
    c.copy(site_id)
  end

  def success(job)
    site.update_attribute(:active, true)
    site.touch_modified_at
  end

  private
  def site
    Site.find(site_id)
  end
  def theme
    Theme.find(theme_id)
  end
end
