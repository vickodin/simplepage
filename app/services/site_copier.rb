class SiteCopier

  SITE = {
        pages: [:name, :content, :system, :kind],
  stylesheets: [:name, :content],
  javascripts: [:name, :content],
      layouts: [:name, :content],
    templates: [:name, :content],
       images: [:name],
        forms: [:name],
    documents: [:name]
  }

  def initialize(theme)
    @tick_counter = 0
    @site = theme.site
  end

  def copy(site, *other)
    if site.class == Fixnum
      @twin = Site.find(site)
    else
      @twin = Site.new(name: site, domain_id: other[0])
      @twin.user_id = other[1]
    end

    if @twin.save
      SiteCopier::SITE.each do |association, structure|
        entities = @site.send(association)

        entities.each do |e|
          twin_entity = @twin.send(association).send(:new)
          structure.each do |field|
            twin_entity.send("#{field}=", e.send(field))
          end
          twin_entity.save

          image_upload(twin_entity, e)     if association == :images
          file_upload(twin_entity, e)     if association == :documents
          form_copy(twin_entity, e, @twin) if association == :forms
          self.tick
        end
      end
    end
    @twin
  end

  protected

  def image_upload(rcv, src)
    if src.photo?
      rcv.remote_photo_url = src.full_url
      rcv.save
    end
  end

  def file_upload(rcv, src)
    if src.attachment?
      rcv.remote_attachment_url = src.full_url
      rcv.save
    end
  end

  def form_copy(rcv, src, twin)
    rcv.user_id = twin.user_id
    src.fields.each do |f|
      field = rcv.fields.new
      field.name = f.name
      field.kind = f.kind
      field.save
    end
    twin.forms << rcv
    rcv.save
  end

  def tick
    @tick_counter += 1
  end
end
