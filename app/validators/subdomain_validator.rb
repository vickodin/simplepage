class SubdomainValidator < ActiveModel::EachValidator

  def validate_each(object, attribute, value)
    return unless value.present?
    reserved_names = %w(www ftp mail pop smtp admin administrator ssl sftp ssh http https about account api app apps auth blog config delete downloads email faq favorites feed feeds help home jobs login log-in logs map maps oauth oauth2 openid privacy register rss signup sign-up signin sign-in sitemap subscribe url user xfn xmpp xxx)
    reserved_names = options[:reserved] if options[:reserved]
    if reserved_names.include?(value)
      object.errors[attribute] << 'cannot be a reserved name'
    end

    object.errors[attribute] << ('must have between 3 and 63 letters') unless (3..63) === value.length
    object.errors[attribute] << ('cannot start with a hyphen') if value =~ /\A\-/
    object.errors[attribute] << ('cannot end with a hyphen') if value =~ /\-\z/
    object.errors[attribute] << ('must be alphanumeric; A-Z, 0-9 or hyphen') unless value =~ /\A[a-z0-9\-]*\z/i
  end
end
