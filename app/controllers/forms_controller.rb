class FormsController < ApplicationController
  before_filter :find_user_form, :only => [:destroy, :show, :edit, :update]

  def index
    breadcrumb('My Forms')

    @forms = current_user.forms#.order('modified_at DESC')
  end

  def new
    breadcrumb('New Form')
    @form = Form.new
  end

  def create
    @form = Form.new(params[:form])
    @form.user = current_user

    if @form.save
      redirect_to form_path(@locale, @form), notice: 'Form was successfully created.'
    else
      breadcrumb('New Form')
      flash.now[:alert] = 'Error'
      render action: "new"
    end
  end

  def update
    if @form.update_attributes(params[:form])
      redirect_to form_path(@locale, @form), notice: 'Form was successfully updated.'
    else
      breadcrumb(@form.name)
      flash.now[:alert] = 'Error'
      render action: "edit"
    end
  end

  def edit
    breadcrumb(@form.name)
  end

  def show
    breadcrumb(@form.name)
  end

  def destroy
    @form.destroy
    redirect_to forms_path(@locale), notice: 'Form was destroyed.'
  end

  private

  def find_user_form
    @form = current_user.forms.find(params[:id])
  end

  def breadcrumb(last)
    base_breadcrumb(
      [
        ["Home", root_url(@locale)],
        ["My Forms", forms_path(@locale)],
      ], last)
  end
end
