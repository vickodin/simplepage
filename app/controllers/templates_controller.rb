class TemplatesController < ApplicationController
  before_filter :find_site
  before_filter :find_structure,     only: [:index, :show]
  before_filter :check_limit,        only: [:new, :create]
  before_filter :find_user_template, only: [:update, :edit, :destroy, :show]

  def index
    breadcrumb('Templates')
  end

  def show
    breadcrumb(@template.name)
    @templates = @site.templates.where(parent_id: @template.id).order('folder DESC, name')
  end

  def new
    breadcrumb('New template')
    @template = @site.templates.new

    if params[:parent_id]
      @parent = @site.templates.find(params[:parent_id])
      @template.parent = @parent
    end
  end

  def create
    @template = @site.templates.new(params[:template])
    unless @template.save
      flash[:alert] = @template.errors.full_messages.join("; ")
    end

    if @template.folder
      if request.referer =~ /templates/
        redirect_to :back
      else
        redirect_to site_templates_path(@locale, @site)
      end
    else
      if @template.save
        redirect_to edit_site_template_path(@locale, @site, @template), notice: 'Created.'
      else
        @parent = @template.parent if @template.parent
        breadcrumb('New template')
        flash.now[:alert] = 'Error'
        render action: "new"
      end
    end
  end

  def edit
    breadcrumb(@template.name)
  end

  def update
    if @template.update_attributes(params[:template])
      redirect_to edit_site_template_path(@locale, @site, @template), notice: 'Saved'
    else
      breadcrumb(@template.name)
      flash.now[:alert] = 'Error'
      render action: "new"
    end
  end

  def destroy
    @template.destroy
    redirect_to site_templates_path(@locale, @site)
  end

  private

  def find_user_template
    @template = @site.templates.find(params[:id])
  end

  def breadcrumb(last)
    base_breadcrumb(
      [
        ["Home", root_url(@locale)],
        ["My sites", sites_path(@locale)],
        [@site.full_name, site_path(@locale, @site)],
        ["Templates", site_templates_path(@locale, @site)]
      ], last)
  end
end
