class PagesController < ApplicationController
  before_filter :find_site
  before_filter :find_structure, :only => [:index]
  before_filter :check_limit, :only => [:new, :create]
  before_filter :find_user_page, :only => [:update, :edit, :destroy, :show]

  def index
    breadcrumb('Pages')
  end

  def show
    breadcrumb(@page.short_public_name)
  end

  def new
    breadcrumb('New page')
    @page = @site.pages.new
  end

  def edit
    breadcrumb(@page.short_public_name)
  end

  def update
    if @page.system
      params[:page].delete(:name)
    end
    if @page.update_attributes(params[:page])
      @page.make_screenshot
      flash[:notice] = 'Saved'
      redirect_to edit_site_page_path(@locale, @site, @page)
    else
      breadcrumb(@page.short_public_name)
      flash.now[:alert] = 'Error'
      render :action => 'edit'
    end
  end

  def create
    @page = @site.pages.new(params[:page])

    respond_to do |format|
      if @page.save

        format.html { redirect_to edit_site_page_path(@locale, @site, @page), notice: 'Page was successfully created.' }
        format.json { render json: @page, status: :created, location: @page }
      else
        breadcrumb('New page')
        flash.now[:alert] = @page.errors.full_messages.join('<br>').html_safe
        format.html { render action: "new", :alert => 'Error' }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @page.destroy

    respond_to do |format|
      format.html { redirect_to site_pages_path(@locale, @site) }
      format.json { head :no_content }
    end
  end

  private

  def find_user_page
    @page = @site.pages.find(params[:id])
  end

  def breadcrumb(last)
    base_breadcrumb(
      [
        ["Home", root_url(@locale)],
        ["My sites", sites_path(@locale)],
        [@site.full_name, site_path(@locale, @site)],
        ["Pages", site_pages_path(@locale, @site)]
      ], last)
  end
end
