class FilesController < ApplicationController
  before_filter :find_site
  before_filter :find_structure, only: [:index, :show]
  before_filter :find_user_file, only: [:update, :edit, :destroy, :show]

  def index
    breadcrumb('Files')
  end

  def show
    breadcrumb(@file.name)
    # overwrite
    @files = @site.documents.where(parent_id: @file.id).order('folder DESC, name')
  end

  def destroy
    @file.destroy
    if request.referer =~ /files/
      redirect_to :back
    else
      redirect_to site_files_path(@locale, @site)
    end
  end

  def create
    @file = @site.documents.new(params[:document])

    unless @file.save
      flash[:alert] = @file.errors.full_messages.join("; ")
    end
    if request.referer =~ /files/
      redirect_to :back
    else
      redirect_to site_files_path(@locale, @site)
    end
  end

  def edit
    breadcrumb(@file.name)
  end

  def update
    params[:document].delete(:parent_id) if params[:document] && params[:document][:parent_id]

    if @file.update_attributes(params[:document])
      redirect_to site_files_path(@locale, @site), :notice => 'Saved'
    else
      breadcrumb(@file.name)
      flash.now[:alert] = 'Error'
      render action: :edit
    end
  end

  private

  def find_user_file
    @file = @site.documents.find(params[:id])
  end

  def breadcrumb(last)
    base_breadcrumb(
      [
        ["Home", root_url(@locale)],
        ["My sites", sites_path(@locale)],
        [@site.full_name, site_path(@locale, @site)],
        ["Files", site_files_path(@locale, @site)]
      ], last)
  end
end
