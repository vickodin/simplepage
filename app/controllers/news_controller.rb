class NewsController < ApplicationController
  skip_before_filter :require_login
  def index
    redirect_to root_url
  end

  def show
    @news = News.friendly.find(params[:id])
  end
end
