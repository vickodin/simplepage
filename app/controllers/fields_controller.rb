class FieldsController < ApplicationController
  before_filter :find_form

  def create
    @field = Field.new(params[:field])
    @field.form = @form
    if @field.save
      redirect_to form_path(@locale, @form), notice: 'Field was successfully created.'
    else
      redirect_to form_path(@locale, @form), alert: 'Something wrong'
    end
  end

  def destroy
    @field = @form.fields.find(params[:id])
    @field.destroy
    redirect_to form_path(@locale, @form)
  end

  private

  def find_form
    @form = current_user.forms.find(params[:form_id])
  end
end
