class SitesController < ApplicationController
  before_filter :find_user_site, :only => [:destroy, :show, :edit, :update]

  def index
    breadcrumb t('breadcrumb.my_sites')

    @sites = current_user.sites.active.order('modified_at DESC')

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @sites }
    end
  end

  def show
    breadcrumb(@site.full_name)
    find_structure

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @site }
    end
  end

  def new
    breadcrumb t('site.new')
    @site = Site.new(theme_id: params[:theme_id])

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @site }
    end
  end

  def create
    @site = Site.new(params[:site])
    @site.user = current_user

    if @site.save
      @site.theme_id.blank? ?  create_simple(@site) : create_from_theme(@site)
      redirect_to site_path(@locale, @site), notice: t('site.success_notice')
    else
      breadcrumb t('site.new')
      flash.now[:alert] = t('site.error')
      render action: :new
    end
  end

  def edit
    breadcrumb(@site.full_name)
  end

  def update
    params[:domain_action] = true if params[:site] && params[:site][:own_domain]

    # notice: we don't allow change system urls of ready sites
    [:name, :domain_id].each { |p| params[:site].delete(p) } if params[:site]

    if @site.update_attributes(params[:site])
      @site.touch
    else
      breadcrumb(@site.full_name)
      flash.now[:alert] = t('site.error')
      return render :edit
    end

    if params[:site][:files]
      if request.referer =~ /files/
        redirect_to :back
      else
        redirect_to site_files_path(@locale, @site)
      end
      return
    end

    return redirect_to site_images_path(@locale, @site) if params[:site][:photos]
    redirect_to sites_path
  end

  def destroy
    @site.active = false
    @site.save

    Delayed::Job.enqueue SiteDestroyJob.new(@site.id)
    respond_to do |format|
      format.html { redirect_to sites_url(@locale) }
      format.json { head :no_content }
    end
  end

  private

  def find_user_site
    @site = current_user.sites.find(params[:id])
  end

  def breadcrumb(last)
    base_breadcrumb(
      [
        [t('breadcrumb.home'), root_url(@locale)],
        [t('breadcrumb.my_sites'), sites_path(@locale)],
      ], last)
  end

  def create_simple(site)
    @page = Page.new
    @page.name = '/'
    @page.system = true

    site.pages << @page
    site.touch
  end

  def create_from_theme(site)
    site.active = false
    site.save
    theme = Theme.active.find(site.theme_id)
    Delayed::Job.enqueue SiteCopierJob.new(theme.id, site.id)
  end
end
