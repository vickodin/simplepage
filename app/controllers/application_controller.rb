class ApplicationController < ActionController::Base
  before_filter :set_locale
  before_filter :require_login

  protect_from_forgery

  protected
  def not_authenticated
    redirect_to root_path(@locale), :alert => t('login_alert')
  end

  private

  def set_locale
    @locale = I18n.locale = params[:locale] || I18n.default_locale
  end

  def find_site
    # FIXME: exception and redirect
    @site = current_user.sites.active.find(params[:site_id])
    return redirect_to root_path(@locale), :alert => t('site.not_found') unless @site
  end

  def find_structure
    @pages        = @site.pages.titled.order('name')
    @stylesheets  = @site.stylesheets.titled.order('name')
    @javascripts  = @site.javascripts.titled.order('name')
    @templates    = @site.templates.root.order('folder DESC, name')
    @layouts      = @site.layouts.titled.order('name')
    @images       = @site.images
    @files        = @site.documents.root.order('folder DESC, name')
  end

  def base_breadcrumb list, last
    if list.length > 1
      if list.last[0] == last
        list.pop
      end
    end
    @breadcrumb = list.push(last)
  end

  def fill_home
    @themes   = Theme.active.order('id desc').limit(3)
    @reviews  = Review.order('id desc').limit(3)
    @news     = News.order('created_at desc').limit(3)
    @stats    = {
        sites: Site.count,
        pages: Page.count,
        forms: Form.count,
           js: Javascript.count,
          css: Stylesheet.count,
       images: Image.count,
    templates: Template.count,
      layouts: Layout.count
    }

    @public   = Site.public.limit(10)
  end

  def check_limit
    return redirect_to( { action: :index }, alert: t('limit_alert')) if @site.send(self.class.to_s.gsub(/Controller/,'').downcase).count > 50
  end
end
