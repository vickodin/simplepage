class Api::BaseController < ActionController::Base
  before_filter :check_access

  private

  def find_site
    # FIXME: exception and redirect
    @site = Site.find(params[:site_id])
  end

  def check_access
    user = User.find_by_api_key!(params[:api_key])
    line = [user.email, user.api_pass, params[:site_id], params[:name], params[:content]].join('')
    puts line
    sign = Digest::MD5.hexdigest(line)
    puts sign
    (return render text:'incorrect sign', status: 404) unless sign == params[:sign]
  end
end
