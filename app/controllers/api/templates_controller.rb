class Api::TemplatesController < Api::BaseController
  before_filter :find_site
  before_filter :find_user_template, only: [:update, :edit, :destroy, :show]

  def update
    @template.content = params[:content]
    @template.save
    render text: 'ok', status: 200
  end

  private

  def find_user_template
    @template = @site.templates.find_by_name!(params[:name])
  end

end
