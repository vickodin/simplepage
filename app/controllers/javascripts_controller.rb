class JavascriptsController < ApplicationController
  before_filter :find_site
  before_filter :find_structure, :only => [:index]
  before_filter :check_limit, :only => [:new, :create]
  before_filter :find_user_javascript, :only => [:update, :edit, :destroy, :show]

  def index
    breadcrumb('Javascripts')
  end

  def show
    breadcrumb(@javascript.full_name)
  end

  def new
    breadcrumb('New javascript')
    @javascript = @site.javascripts.new
  end

  def create
    @javascript = @site.javascripts.new(params[:javascript])

    if @javascript.save
      redirect_to edit_site_javascript_path(@locale, @site, @javascript), notice: 'Created.'
    else
      breadcrumb('New javascript')
      flash[:alert] = 'Error'
      render action: "new"
    end
  end

  def edit
    breadcrumb(@javascript.full_name)
  end

  def update
    if @javascript.update_attributes(params[:javascript])
      redirect_to edit_site_javascript_path(@locale, @site, @javascript), notice: 'Saved'
    else
      breadcrumb(@javascript.full_name)
      flash.now[:alert] = 'Error'
      render :action => 'edit'
    end
  end

  def destroy
    @javascript.destroy
    redirect_to site_javascripts_path(@locale, @site)
  end

  private

  def find_user_javascript
    @javascript = @site.javascripts.find(params[:id])
  end

  def breadcrumb(last)
    base_breadcrumb(
      [
        ["Home", root_url(@locale)],
        ["My sites", sites_path(@locale)],
        [@site.full_name, site_path(@locale, @site)],
        ["Javascripts", site_javascripts_path(@locale, @site)]
      ], last)
  end
end
