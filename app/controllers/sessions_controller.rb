class SessionsController < ApplicationController
  skip_before_filter :require_login, :except => [:destroy]

  def new
    @user = User.new
  end

  def create
    if @user = login(params[:email], params[:password], params[:remember_me])
      #redirect_back_or_to(root_url, :notice => 'Login successful.')
      redirect_back_or_to root_url(@locale)
    else
      flash[:alert] = 'Login failed.'
      redirect_to root_url(@locale)
    end
  end

  def destroy
    logout
    #redirect_to(root_url, :notice => 'Logged out!')
    redirect_to root_url(@locale)
  end
end
