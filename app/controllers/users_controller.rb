class UsersController < ApplicationController
  skip_before_filter :require_login

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])

    if @user.save
      redirect_to root_url(@locale), notice: 'Check the mail for activation email, please.'
    else
      flash.now[:alert] = @user.errors.full_messages.join('<br>').html_safe
      flash.now[:user_creation_action] = true
      fill_home
      render :template => '/home/index'
    end
  end

  def activate
    if (@user = User.load_from_activation_token(params[:id]))
      @user.activate!
      auto_login(@user)
      redirect_to(root_url(@locale), :notice => 'Welcome! Your account was successfully activated.')
    else
      not_authenticated
    end
  end
end
