class ImagesController < ApplicationController
  before_filter :find_site
  before_filter :find_structure, :only => [:index]
  before_filter :check_limit, :only => [:new, :create]
  before_filter :find_user_image, :only => [:update, :edit, :destroy, :show]

  def index
    breadcrumb('Images')
  end

  def show
    breadcrumb(@image.short_public_name)
  end

  def new
    breadcrumb('New image')
    @image = @site.images.new
  end

  def create
    @image = @site.images.new(params[:image])

    if @image.save
      redirect_to site_image_path(@locale, @site, @image), notice: 'Created.'
    else
      breadcrumb('New image')
      flash.now[:alert] = 'Error'
      render action: "new"
    end
  end

  def edit
    breadcrumb(@image.short_public_name)
  end

  def update
    if @image.update_attributes(params[:image])
      redirect_to site_images_path(@locale, @site), notice: 'Saved.'
    else
      breadcrumb(@image.short_public_name)
      flash.now[:alert] = 'Error'
      render action: "edit"
    end
  end

  def destroy
    @image.destroy
    redirect_to site_images_path(@locale, @site)
  end

  private

  def find_user_image
    @image = @site.images.find(params[:id])
  end

  def breadcrumb(last)
    base_breadcrumb(
      [
        ["Home", root_url(@locale)],
        ["My sites", sites_path(@locale)],
        [@site.full_name, site_path(@locale, @site)],
        ["Images", site_images_path(@locale, @site)]
      ], last)
  end
end
