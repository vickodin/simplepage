class LayoutsController < ApplicationController
  before_filter :find_site
  before_filter :find_structure, :only => [:index]
  before_filter :check_limit, :only => [:new, :create]
  before_filter :find_user_layout, :only => [:update, :edit, :destroy, :show]

  def index
    breadcrumb('Layouts')
  end

  def show
    breadcrumb(@layout.name)
  end

  def new
    @layout = @site.layouts.new
    breadcrumb('New layout')
  end

  def create
    @layout = @site.layouts.new(params[:layout])
    if @layout.save
      redirect_to edit_site_layout_path(@locale, @site, @layout), notice: 'Created.'
    else
      breadcrumb('New layout')
      flash.now[:alert] = 'Error'
      render action: "new"
    end
  end

  def edit
    breadcrumb(@layout.name)
  end

  def update
    if @layout.update_attributes(params[:layout])
      redirect_to edit_site_layout_path(@locale, @site, @layout), notice: 'Saved'
    else
      breadcrumb(@layout.name)
      flash.now[:alert] = 'Error'
      render action: 'edit'
    end
  end

  def destroy
    @layout.destroy
    redirect_to site_layouts_path(@locale, @site)
  end

  private

  def find_user_layout
    @layout = @site.layouts.find(params[:id])
  end

  def breadcrumb(last)
    base_breadcrumb(
      [
        ["Home", root_url(@locale)],
        ["My sites", sites_path(@locale)],
        [@site.full_name, site_path(@locale, @site)],
        ["Layouts", site_layouts_path(@locale, @site)]
      ], last)
  end
end
