class ThemesController < ApplicationController
  skip_before_filter :require_login
  layout "theme"

  def show
    @theme = Theme.active.find(params[:id])
  end
end
