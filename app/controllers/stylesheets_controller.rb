class StylesheetsController < ApplicationController
  before_filter :find_site
  before_filter :find_structure, :only => [:index]
  before_filter :check_limit, :only => [:new, :create]
  before_filter :find_user_stylesheet, :only => [:update, :edit, :destroy, :show]

  def index
    breadcrumb('Stylesheets')
  end

  def new
    breadcrumb('New stylesheet')
    @stylesheet = @site.stylesheets.new
  end

  def show
    breadcrumb(@stylesheet.full_name)
  end

  def create
    @stylesheet = @site.stylesheets.new(params[:stylesheet])

    if @stylesheet.save
      redirect_to edit_site_stylesheet_path(@locale, @site, @stylesheet), notice: 'Created.'
    else
      breadcrumb('New stylesheet')
      flash.now[:alert] = 'Error'
      render action: "new"
    end
  end

  def edit
    breadcrumb(@stylesheet.full_name)
  end

  def update
    if @stylesheet.update_attributes(params[:stylesheet])
      redirect_to edit_site_stylesheet_path(@locale, @site, @stylesheet), notice: 'Saved'
    else
      flash.now[:alert] = 'Error'
      breadcrumb(@stylesheet.full_name)
      render action: 'edit'
    end
  end

  def destroy
    @stylesheet.destroy
    redirect_to site_stylesheets_path(@locale, @site)
  end

  private

  def find_user_stylesheet
    @stylesheet = @site.stylesheets.find(params[:id])
  end

  def breadcrumb(last)
    base_breadcrumb(
      [
        ["Home", root_url(@locale)],
        ["My sites", sites_path(@locale)],
        [@site.full_name, site_path(@locale, @site)],
        ["Stylesheets", site_stylesheets_path(@locale, @site)]
      ], last)
  end
end
