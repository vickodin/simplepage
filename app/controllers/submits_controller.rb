class SubmitsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  skip_before_filter :require_login, only: [:create, :success]

  def index
    @form = current_user.forms.find(params[:form_id])
    @fields = @form.fields
    @submits = @form.submits.includes(:answers).order('created_at DESC')
    breadcrumb('Submits')
  end

  def create
    form = Form.find_by_uuid!(params[:uuid])
    submit = Submit.new
    submit.form = form
    submit.save

    fake = true

    form.fields.each do |field|
      unless params[field.name].blank?
        answer = Answer.new
        answer.value   = params[field.name]
        answer.submit  = submit
        answer.field   = field
        answer.save

        fake = false
      end
    end

    if form.captcha?
      fake = true unless verify_recaptcha(model: submit)
    end

    # spam catcher
    if params[:email_1_1] && !params[:email_1_1].blank?
      fake = true
    end

    unless fake
      SubmitMailer.submit(submit).deliver
    end

    url = form.back_to_url(params[:back_to_url])
    unless url.blank?
      redirect_to url
    else
      redirect_to submit_success_url(@locale)
    end
  end

  def destroy
    @form   = current_user.forms.find(params[:form_id])
    @submit = @form.submits.find(params[:id])
    @submit.destroy
    redirect_to form_submits_path(@locale, @form)
  end

  private

  def breadcrumb(last)
    base_breadcrumb(
      [
        ["Home", root_url(@locale)],
        ["My Forms", forms_path(@locale)],
        [@form.name, form_path(@locale, @form)]
      ], last)
  end
end
