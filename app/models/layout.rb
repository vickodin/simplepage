# == Schema Information
#
# Table name: layouts
#
#  id         :integer          not null, primary key
#  name       :string(255)      default(""), not null
#  content    :text
#  site_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Layout < ActiveRecord::Base
  extend SiteToucher
  extend TitledScope
  include Urler

  belongs_to :site

  attr_accessible :content, :name
  validates :name, :uniqueness => {:scope => :site_id}
  validates :name, :presence => true
  validates :name, :format => {:with => /\A[^~`@#\$%^&*()\[\]{}+=|\\:;"'<>,.]+\z/}

  full_rebase_timestamp(:after_save, :before_destroy)
end
