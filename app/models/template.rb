# == Schema Information
#
# Table name: templates
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  content    :text
#  site_id    :integer
#  created_at :datetime
#  updated_at :datetime
#  folder     :boolean          default(FALSE), not null
#  parent_id  :integer
#

class Template < ActiveRecord::Base
  extend TitledScope
  extend SiteToucher
  include Urler
  include Nester

  belongs_to :site
  belongs_to :parent, class_name: 'Template'
  has_many :children, class_name: 'Template', foreign_key: :parent_id, dependent: :destroy

  attr_accessible :content, :name, :folder, :parent_id

  validates :name, uniqueness: { scope: [:site_id, :parent_id] }
  validates :name, presence: true
  validates :name, format: { with: /\A[^~`@#\$%^&*()\[\]{}+=|\\:;"'<>,.]+\z/ }

  validate :check_parent_id

  full_rebase_timestamp(:after_save, :before_destroy)

  scope :root, -> { where(parent_id: nil) }

  private

  def check_parent_id
    unless self.parent_id.blank?
      if parent && (parent.site.id == self.site_id)
        return true
      end
      errors.add(:base, I18n.t('template.errors.parent_id'))
    end
  end
end
