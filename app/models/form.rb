# == Schema Information
#
# Table name: forms
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  name           :string(255)
#  uuid           :string(255)      not null
#  created_at     :datetime
#  updated_at     :datetime
#  redirect       :string(255)
#  notify_subject :string(255)
#  captcha        :boolean          default(FALSE), not null
#

class Form < ActiveRecord::Base
  belongs_to :user

  has_many :fields,   dependent: :destroy
  has_many :submits,  dependent: :destroy

  # TODO: make dependent hook
  has_and_belongs_to_many :sites

  attr_accessible :name, :redirect, :notify_subject
  #validates :name, uniqueness: {scope: :user_id}, allow_blank: true

  before_create :make_uuid

  def back_to_url(*addresses)
    url = self.redirect
    addresses.each do |address|
      (url = address; break) unless address.blank?
    end
    url
  end

  def locale_path
    if self.new_record?
      UrlGenerator.new.forms_path(@locale)
    else
      UrlGenerator.new.form_path(@locale, self.id)
    end
  end

  private

  def make_uuid
    self.uuid = SecureRandom.uuid
  end
end
