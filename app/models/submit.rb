# == Schema Information
#
# Table name: submits
#
#  id         :integer          not null, primary key
#  form_id    :integer
#  ip         :string(255)
#  ua         :string(255)
#  referer    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Submit < ActiveRecord::Base
  belongs_to :form
  has_many :answers, dependent: :destroy

  attr_accessible :ip, :referer, :ua
end
