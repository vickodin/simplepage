# == Schema Information
#
# Table name: users
#
#  id                              :integer          not null, primary key
#  email                           :string(255)
#  crypted_password                :string(255)
#  salt                            :string(255)
#  created_at                      :datetime
#  updated_at                      :datetime
#  remember_me_token               :string(255)
#  remember_me_token_expires_at    :datetime
#  reset_password_token            :string(255)
#  reset_password_token_expires_at :datetime
#  reset_password_email_sent_at    :datetime
#  activation_state                :string(255)
#  activation_token                :string(255)
#  activation_token_expires_at     :datetime
#  locale                          :string(255)      default("en"), not null
#  api_key                         :string(255)
#  api_pass                        :string(255)
#

class User < ActiveRecord::Base
  authenticates_with_sorcery!

  has_many :sites, :dependent => :destroy
  has_many :forms, :dependent => :destroy

  attr_accessible :email, :password, :password_confirmation

  validates_length_of :password, :minimum => 3, :message => "password must be at least 3 characters long", :if => :password
  validates_confirmation_of :password, :message => "should match confirmation", :if => :password
  validates :email, :presence => true, :uniqueness => true

  def locale_path
    if self.new_record?
      UrlGenerator.new.users_path(@locale)
    else
      UrlGenerator.new.user_path(@locale, self.id)
    end
  end
end
