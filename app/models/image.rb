# == Schema Information
#
# Table name: images
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  photo      :string(255)
#  site_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

class Image < ActiveRecord::Base
  extend SiteToucher
  extend TitledScope
  include Urler

  belongs_to :site

  attr_accessible :name, :photo
  validates :name, :uniqueness => { scope: :site_id }, :allow_blank => true

  # TODO: make photo_path for skip carrierwave at render
  #after_save :set_photo_path

  mount_uploader :photo, ImageUploader

  full_rebase_timestamp(:after_save, :before_destroy)

  def full_url
    "#{site.full_url}#{self.photo_url}" if self.photo?
  end

  def public_name
    self.name.blank? ? 'No name': self.name
  end

  def short_public_name
    self.public_name.truncate(30)
  end
end
