# == Schema Information
#
# Table name: answers
#
#  id         :integer          not null, primary key
#  submit_id  :integer
#  field_id   :integer
#  value      :text
#  created_at :datetime
#  updated_at :datetime
#

class Answer < ActiveRecord::Base
  belongs_to :submit
  belongs_to :field

  attr_accessible :value
end
