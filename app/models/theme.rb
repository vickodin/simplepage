# == Schema Information
#
# Table name: themes
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  site_id     :integer
#  screenshot  :string(255)
#  active      :boolean          default(FALSE)
#  created_at  :datetime
#  updated_at  :datetime
#

class Theme < ActiveRecord::Base
  belongs_to :site

  validates :name, presence: true, uniqueness: true
  attr_accessible :name, :description, :site_id, :screenshot, :active
  mount_uploader :screenshot, DiskUploader

  scope :active, -> { where(active: true) }
end
