# == Schema Information
#
# Table name: news
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#  slug        :string(255)
#  preview     :text
#

class News < ActiveRecord::Base
  extend FriendlyId

  attr_accessible :description, :name, :slug, :preview

  friendly_id :name, use: :slugged

end
