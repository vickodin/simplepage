# == Schema Information
#
# Table name: pages
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  site_id    :integer
#  content    :text
#  system     :boolean          default(FALSE), not null
#  kind       :string(255)      default("html"), not null
#  screenshot :string(255)
#  sitemap    :boolean          default(FALSE), not null
#

class Page < ActiveRecord::Base
  extend Enumerize
  include Urler


  KIND   = ['html', 'txt']

  belongs_to :site
  has_one :cacher, as: :cacheable, dependent: :destroy


  validates :name, :uniqueness => {:scope => [:site_id, :kind]}
  validates :name, :presence => true
  validates :name, :format => {:with => /\A[^~`@#\$%^&*()\[\]{}+=|\\:;"'<>,.]+\z/}
  validates :kind, :inclusion => { :in => Page::KIND}

  enumerize :kind, :in => Page::KIND, :predicates => true

  scope :titled, -> { select('id, name, site_id, system, kind, screenshot') }

  #before_save :make_screenshot

  attr_accessible :content, :name, :kind, :screenshot, :sitemap

  mount_uploader :screenshot, ScreenshotUploader

  def full_name
    path = '/'
    unless self.name == '/'
      path = "/#{self.name}"
      unless self.html?
        path +='.'+self.kind
      end
    end
    return path
  end

  def public_name
    if self.system && self.name == '/'
      return 'Index page'
    else
      unless self.html?
        return "#{self.name}.#{self.kind}"
      else
        return self.name
      end
    end
  end

  def short_public_name
    self.public_name.truncate(30)
  end
  def full_url
    "#{self.site.full_url}#{self.full_name}"
  end

  def make_screenshot
    delay.run_screenshot
  end

  def run_screenshot
    url = Prtscr.get(url: full_url, width: 1280, height: 1280, scale: 100, key: 'fs6p4wwO', secret: 'IWYEqW0c6hcJ5ashw/4D49xW')
    self.remote_screenshot_url = url

    if self.save
      puts "YES screenshot"
    else
      puts "NO screenshot"
    end
  end
end
