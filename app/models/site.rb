# == Schema Information
#
# Table name: sites
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  user_id     :integer
#  domain_id   :integer
#  created_at  :datetime
#  updated_at  :datetime
#  own_domain  :string(255)      default(""), not null
#  more        :text
#  compress    :boolean          default(TRUE)
#  modified_at :datetime         default(2017-01-18 15:04:23 UTC), not null
#  public      :boolean          default(FALSE), not null
#  active      :boolean          default(TRUE), not null
#  favicon     :string(255)
#  blocked     :boolean          default(FALSE), not null
#

class Site < ActiveRecord::Base
  belongs_to :user
  belongs_to :domain

  has_many :stylesheets,  dependent: :destroy
  has_many :javascripts,  dependent: :destroy
  has_many :pages,        dependent: :destroy
  has_many :templates,    dependent: :destroy
  has_many :layouts,      dependent: :destroy
  has_many :images,       dependent: :destroy
  has_many :documents,    dependent: :destroy

  # TODO: make dependent hook
  has_and_belongs_to_many :forms, :conditions => proc { "forms.user_id = #{self.user_id}" }, uniq: true

  #serialize :more, Hash
  attr_accessible :name, :domain_id, :own_domain, :compress, :public, :form_ids, :theme_id, :files, :photos, :parent_id, :favicon, :blocked
  accepts_nested_attributes_for :documents, :allow_destroy => true

  attr_accessor :theme_id, :files, :photos, :parent_id



  validates :name,        :uniqueness => {:scope => :domain_id}, :subdomain => true
  validates :name,        :presence => true
  validates :name,        :format => {:with => /(\A\w+[a-z0-9\-]+\w+\z)/}
  validates :name,        :length => { :in => 3..64 }
  validates :domain,      :presence => true
  validates :own_domain,  :uniqueness => true, :allow_blank => true

  mount_uploader :favicon, FaviconUploader

  scope :public, -> { where(public: true) }
  scope :active, -> { where(active: true) }



  def full_name
    if self.own_domain.blank?
      self.name + '.' + self.domain.name
    else
      self.own_domain
    end
  end

  def full_url
    "http://#{self.full_name}"
  end

  def touch_modified_at
    self.modified_at = Time.current
    self.save
  end
  alias :touch :touch_modified_at

  # INFO: files upload
  before_update do
    unless self.files.blank?
      self.files.each do |file|
        self.documents.create(attachment: file, name: file.original_filename, parent_id: self.parent_id)
      end
      self.files = []
    end

    unless self.photos.blank?
      self.photos.each do |file|
        self.images.create(photo: file, name: file.original_filename)
      end
      self.photos = []
    end
  end

  def locale_path
    if self.new_record?
      UrlGenerator.new.sites_path(@locale)
    else
      UrlGenerator.new.site_path(@locale, self.id)
    end
  end
end
