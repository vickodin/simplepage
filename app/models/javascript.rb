# == Schema Information
#
# Table name: javascripts
#
#  id         :integer          not null, primary key
#  content    :text
#  created_at :datetime
#  updated_at :datetime
#  site_id    :integer
#  name       :string(255)      default(""), not null
#

class Javascript < ActiveRecord::Base
  extend TitledScope
  include Urler

  belongs_to :site
  has_one :cacher, as: :cacheable, dependent: :destroy

  attr_accessible :content, :name
  validates :name, :uniqueness => {:scope => :site_id}
  validates :name, :presence => true
  validates :name, :format => {:with => /\A[^~`@#\$%^&*()\[\]{}+=|\\:;"'<>,.]+\z/}

  def full_name
    "#{self.name}.js"
  end

  def full_url
    "#{self.site.full_url}/#{self.full_name}"
  end

end
