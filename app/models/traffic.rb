# == Schema Information
#
# Table name: traffics
#
#  id         :integer          not null, primary key
#  site_id    :integer
#  hosts      :integer
#  hits       :integer
#  visitors   :integer
#  created_at :datetime
#  updated_at :datetime
#

class Traffic < ActiveRecord::Base
  belongs_to :site
  attr_accessible :hits, :hosts, :visitors
end
