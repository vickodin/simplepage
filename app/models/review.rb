# == Schema Information
#
# Table name: reviews
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  content    :text
#  photo      :string(255)
#  url        :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Review < ActiveRecord::Base
  attr_accessible :content, :name, :photo, :url
  mount_uploader :photo, ReviewUploader
end
