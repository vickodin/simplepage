# == Schema Information
#
# Table name: documents
#
#  id         :integer          not null, primary key
#  site_id    :integer
#  name       :string(255)
#  attachment :string(255)
#  created_at :datetime
#  updated_at :datetime
#  folder     :boolean          default(FALSE), not null
#  parent_id  :integer
#

class Document < ActiveRecord::Base
  include Nester

  belongs_to :site
  belongs_to :parent, class_name: 'Document'
  has_many :children, class_name: 'Document', foreign_key: :parent_id, dependent: :destroy

  attr_accessible :name, :attachment, :parent_id, :folder

  validates :site, presence: true
  validates :name, uniqueness: { scope: [:site_id, :parent_id] }, allow_blank: true

  validate :check_parent_id

  mount_uploader :attachment, DiskUploader

  scope :root, -> { where(parent_id: nil) }

  def full_url
    if self.attachment?
      #return "#{site.full_url}/#{self.attachment.path}"
      return "#{site.full_url}#{self.attachment_url}"
    else
      return nil
    end
  end

  def locale_path
    if self.new_record?
      UrlGenerator.new.site_files_path(@locale, self.site)
    else
      UrlGenerator.new.site_file_path(@locale, self.site, self.id)
    end
  end

  private

  def check_parent_id
    unless self.parent_id.blank?
      if parent && (parent.site.id == self.site_id)
        return true
      end
      errors.add(:base, I18n.t('document.errors.parent_id'))
    end
  end

end
