# == Schema Information
#
# Table name: fields
#
#  id         :integer          not null, primary key
#  form_id    :integer
#  name       :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#  kind       :string(255)
#

class Field < ActiveRecord::Base
  belongs_to :form

  attr_accessible :name, :kind
  validates :name, uniqueness: {scope: :form_id}
  validates :name, presence: true
  validates :name, :exclusion => { :in => %w(back_to_url) }

  extend Enumerize
  enumerize :kind, in: [:text, :select, :textarea], predicates: true, default: :text

  def locale_path
    if self.new_record?
      UrlGenerator.new.form_fields_path(@locale, self.form)
    else
      UrlGenerator.new.form_field_path(@locale, self.form, self.id)
    end
  end
end
