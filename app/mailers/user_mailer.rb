class UserMailer < ActionMailer::Base
  default from: "Simple Page Support <support@simplepage.biz>"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.activation_needed_email.subject
  #
  def activation_needed_email(user)
    @user = user
    @url  = "http://#{Simplepage.config.base_domain}/users/#{user.activation_token}/activate"
    mail(:to => user.email, :subject => "Welcome to SimplePage.biz")
  end

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.activation_success_email.subject
  #
  def activation_success_email(user)
    @user = user
    @url  = "http://#{Simplepage.config.base_domain}"
    mail(:to => user.email, :subject => "Your account is now activated")
  end

  def reset_password_email(user)
    @user = user
    @url  = "http://#{Simplepage.config.base_domain}/resets/#{user.reset_password_token}/edit"
    mail(:to => user.email, :subject => "Your password reset request")
  end
end
