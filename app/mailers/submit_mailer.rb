class SubmitMailer < ActionMailer::Base
  default from: "Simple Page Support <support@simplepage.biz>"
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.submit_mailer.submit.subject
  #
  def submit(s)
    @submit = s

    mail(to: s.form.user.email, subject: s.form.notify_subject.blank? ? "New submit to #{s.form.name}" : s.form.notify_subject)
  end
end
