Simplepage::Application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  scope "(:locale)", :locale => /en|ru/ do
    root :to => 'home#index'
    resources :sites do
      resources :stylesheets
      resources :javascripts
      resources :pages
      resources :templates
      resources :layouts
      resources :images
      resources :files
    end

    resources :forms do
      resources :submits
      resources :fields
    end

    resources :users do
      member do
        get :activate
      end
    end

    resources :sessions
    resources :resets
    resources :news, :only => [:index, :show]
    resources :reviews, :only => [:index, :create]

    resources :themes, only: [:show]

    get "login" => "sessions#new", :as => "login"
    post "logout"=> "sessions#destroy", :as => "logout"

    get 'contact_us' => "home#contact_us", as: 'contact_us'

    get   "submit/success"  => "submits#success", as: 'submit_success'
    post  "submit/:uuid"    => "submits#create"

    get 'help'  => "home#help", as: 'help'
    get 'rules' => "home#rules", as: 'rules'
  end

  namespace :api do
    resources :sites, only: [] do
      resources :templates, only: [] do
        post 'update', on: :collection
      end
    end
  end
end
