require 'spec_helper'

describe SubmitsController do
  render_views

  def post_data(uuid)
    { uuid: uuid,
      name: 'name',
      email: 'email@email.me',
      phone: '1-800-NO-COPIES' }
  end

  context "Site submits" do
    before(:each) do
      @form = FactoryGirl.create(:form)
      ['name', 'email', 'phone'].each do |name|
        @form.fields << FactoryGirl.build(:field, form: @form, name: name)
      end
    end
    
    it "creates success submit" do
      post_request = -> (uuid) {
        post :create, {
          uuid: uuid,
          name: 'name',
          email: 'email@email.me',
          phone: '1-800-NO-COPIES'
        }
      }
      expect { post_request.call(@form.uuid) }.to change(Submit, :count).by(1)
    end

    it "makes new answers" do
      expect { post :create, post_data(@form.uuid) }.to change(Answer, :count).by(3)
    end

    it "returns usual redirect" do
      post :create, post_data(@form.uuid)
      response.should redirect_to submit_success_path($locale)
    end

    it "returns form's redirect" do
      redirect_url = 'http://google.com'
      @form.redirect = redirect_url
      @form.save

      post :create, post_data(@form.uuid)
      response.should redirect_to redirect_url
    end

    it "returns back_to_url redirect" do
      back_to_url = 'http://google.com'
      post :create, post_data(@form.uuid).merge(back_to_url: back_to_url)
      response.should redirect_to back_to_url
    end

    it "allow access to submit_success_path" do
      get :success
      response.should be_success
    end

  end
end
