require 'spec_helper'

describe SitesController do
  render_views

  context "access control" do
    it "redirect for guests" do
      get :index
      response.should redirect_to(root_path($locale))
    end
    it "return success for users" do
      user = FactoryGirl.create(:user)
      login_user user
      get :index
      response.should be_success
    end
  end

  context "CRUD" do
    let(:new_site_request) { -> { post :create, {site: {name: 'test', domain_id: Domain.last.id}} } }

    before(:each) do
      @user = FactoryGirl.create(:user)
      login_user
      FactoryGirl.create(:domain)
    end

    describe "new site action" do
      it "returns form" do
        get :new
        response.should be_success
        response.body.should have_content(I18n.t('site.new'))
      end
    end

    describe "site creation" do
      it "changes the Sites count" do
        expect(new_site_request).to change(Site, :count).by(1)
      end

      it "changes the Pages count" do
        expect(new_site_request).to change(Page, :count).by(1)
      end

      it "makes redirect to new site" do
        new_site_request.call
        response.should redirect_to(site_path($locale, Site.last))
        flash[:notice].should eql(I18n.t('site.success_notice'))
      end
    end

    describe "site destroy" do
      it "changes the Sites count" do
        new_site_request.call
        expect { post :destroy, {id: Site.last.id} }.to change(Site.active, :count).by(-1)
      end

      #it "changes the Pages count" do
      #  new_site_request.call
      #  pages_count = Site.last.pages.count
      #  expect { post :destroy, {id: Site.last.id} }.to change(Page, :count).by(-pages_count)
      #end

      it "responds with 404 when foreign user" do
        new_site_request.call
        FactoryGirl.create(:site)
        expect{ post :destroy, {id: Site.last.id} }.to raise_error(ActiveRecord::RecordNotFound)
      end
    end

    describe "update" do
      it "returns form for edit" do
        site = FactoryGirl.create(:site, user: @user)
        get :edit, id: site.id
        response.should be_success
        response.body.should have_content(site.full_name)
      end

      it "still has old name and new other values" do
        site = FactoryGirl.create(:site, name: 'original', user: @user)
        put :update, { id: site.id, site: { name: 'newname', public: true, compress: false} }
        site.reload
        site.name.should eq 'original'
        site.public.should be_true
        site.compress.should be_false
      end
    end
  end
end
