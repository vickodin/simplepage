require 'spec_helper'

describe UsersController do
  render_views

  context "registration" do
    let(:new_user_request) { -> { post :create, {user: {email: 'test@example.com', password: 'xxx', password_confirmation: 'xxx'}} } }

    it 'makes a new user' do
      expect(new_user_request).to change(User, :count).by(1)
    end

    it "makes redirect" do
      new_user_request.call
      response.should redirect_to root_path($locale)
    end
  end
end
