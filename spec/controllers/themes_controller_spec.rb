require 'spec_helper'

describe ThemesController do
  let (:theme) { FactoryGirl.create(:theme, active: true) }

  describe "GET 'show'" do
    it "returns http success" do
      get 'show', id: theme.id
      response.should be_success
    end
  end

end
