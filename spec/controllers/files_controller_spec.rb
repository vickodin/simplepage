require 'spec_helper'

describe FilesController do
	render_views

	before(:all) do
		
	end

  describe "GET 'index'" do
    it "returns http success" do
    	user = FactoryGirl.create(:user)
    	login_user user
    	@site = FactoryGirl.create(:site, user: user)

      get 'index', site_id: @site.id
      response.should be_success
    end
  end

end
