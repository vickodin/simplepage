# == Schema Information
#
# Table name: fields
#
#  id         :integer          not null, primary key
#  form_id    :integer
#  name       :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#  kind       :string(255)
#

require 'spec_helper'

describe Field do
  it_behaves_like "belongs_to", [:form]
  context "name"
    it "valid" do
      field = FactoryGirl.build(:field)
      field.should be_valid
      expect { field.save }.to change(Field, :count).by(1)
    end
    it "checks for exclusions" do
      field = FactoryGirl.build(:field, name: 'back_to_url')
      field.should_not be_valid
      field.should have(1).error_on(:name)
    end
end
