# == Schema Information
#
# Table name: domains
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Domain do
  context 'structure' do
    describe 'associations' do
      it { should have_many :sites }
    end
  end
end
