# == Schema Information
#
# Table name: documents
#
#  id         :integer          not null, primary key
#  site_id    :integer
#  name       :string(255)
#  attachment :string(255)
#  created_at :datetime
#  updated_at :datetime
#  folder     :boolean          default(FALSE), not null
#  parent_id  :integer
#

require 'spec_helper'

describe Document do
  it_behaves_like "belongs_to", [:site]

  context "validations" do
    let (:document) { FactoryGirl.build(:document) }

    it "valid" do
      document.should be_valid
    end

    it "when not unique name" do
      document.save
      document2 = document.dup
      document2.should_not be_valid
    end
  end
end
