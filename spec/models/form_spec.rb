# == Schema Information
#
# Table name: forms
#
#  id             :integer          not null, primary key
#  user_id        :integer
#  name           :string(255)
#  uuid           :string(255)      not null
#  created_at     :datetime
#  updated_at     :datetime
#  redirect       :string(255)
#  notify_subject :string(255)
#  captcha        :boolean          default(FALSE), not null
#

require 'spec_helper'

describe Form do
  it_behaves_like "belongs_to",               [:user]
  it_behaves_like "has_many",                 [:fields, :submits]
  it_behaves_like "has_and_belongs_to_many",  [:sites]

  context "validations" do
    let (:form) { FactoryGirl.create(:form) }

    it "valid" do
      form.should be_valid
    end

    it "allow not uniqueness" do
      form2 = form.dup
      form2.should be_valid
    end

    it "allow no names" do
      form.name = nil
      form.should be_valid
    end

    it "creates uuid" do
      form.uuid.should_not be_blank
    end

    it "has field" do
      3.times { FactoryGirl.create(:field, form: form) }
      form.fields.length.should == 3
    end
  end
end
