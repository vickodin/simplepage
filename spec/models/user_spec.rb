# == Schema Information
#
# Table name: users
#
#  id                              :integer          not null, primary key
#  email                           :string(255)
#  crypted_password                :string(255)
#  salt                            :string(255)
#  created_at                      :datetime
#  updated_at                      :datetime
#  remember_me_token               :string(255)
#  remember_me_token_expires_at    :datetime
#  reset_password_token            :string(255)
#  reset_password_token_expires_at :datetime
#  reset_password_email_sent_at    :datetime
#  activation_state                :string(255)
#  activation_token                :string(255)
#  activation_token_expires_at     :datetime
#  locale                          :string(255)      default("en"), not null
#  api_key                         :string(255)
#  api_pass                        :string(255)
#

require 'spec_helper'

describe User do
  it_behaves_like "has_many", [:sites, :forms]

  context "Structure" do
    before(:each) do
      @user = User.new(email: 'test@example.org', password: '123', password_confirmation: '123')
    end

    it "valid" do
      user = FactoryGirl.build :user
      user.should be_valid
    end

    it "valid" do
      @user.should be_valid
    end

    it "makes new user" do
      expect { @user.save }.to change(User, :count).by(1)
    end

    it "checks for email uniqueness" do
      @user.save
      user = FactoryGirl.build(:user, email: @user.email)
      user.should have(1).error_on(:email)
    end

    it "checks for email presence" do
      user = FactoryGirl.build(:user, email: nil)
      user.should have(1).error_on(:email)
    end
  end

end
