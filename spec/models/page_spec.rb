# == Schema Information
#
# Table name: pages
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  site_id    :integer
#  content    :text
#  system     :boolean          default(FALSE), not null
#  kind       :string(255)      default("html"), not null
#  screenshot :string(255)
#  sitemap    :boolean          default(FALSE), not null
#

require 'spec_helper'

describe Page do
  it_behaves_like "belongs_to", [:site]
  it_behaves_like "has_one",    [:cacher]
  context "validations" do
    let (:page) { FactoryGirl.create(:page) }

    it "checks names for uniqueness" do
      another_page = page.dup
      another_page.should_not be_valid
      another_page.should have(1).error_on(:name)
    end

    it "checks names for presence" do
      page.name = nil
      page.should_not be_valid
    end

    it "checks for kind" do
      page.should be_valid
      page.kind = 'stop'
      page.should_not be_valid
    end

  end
end
