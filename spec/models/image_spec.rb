# == Schema Information
#
# Table name: images
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  photo      :string(255)
#  site_id    :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Image do
  it_behaves_like "belongs_to", [:site]
  
  context "validations" do
    let (:image) { FactoryGirl.create(:image) }

    it "checks names for uniqueness" do
      another_image = image.dup
      another_image.valid?.should be_false
      another_image.should have(1).error_on(:name)
    end

    it "allows no names" do
      image.name = nil
      image.save

      another_image = image.dup
      another_image.valid?.should be_true
    end

    it "allows equal names for different sites" do
      another_image = image.dup
      another_image.site = FactoryGirl.create(:site)
      image.name.should eq another_image.name
      another_image.should be_valid
    end
  end
end
