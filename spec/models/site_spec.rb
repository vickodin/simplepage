# == Schema Information
#
# Table name: sites
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  user_id     :integer
#  domain_id   :integer
#  created_at  :datetime
#  updated_at  :datetime
#  own_domain  :string(255)      default(""), not null
#  more        :text
#  compress    :boolean          default(TRUE)
#  modified_at :datetime         default(2017-01-18 15:04:23 UTC), not null
#  public      :boolean          default(FALSE), not null
#  active      :boolean          default(TRUE), not null
#  favicon     :string(255)
#  blocked     :boolean          default(FALSE), not null
#

require 'spec_helper'

describe Site do
  it_behaves_like "belongs_to", [:user, :domain]
  it_behaves_like "has_many",   [:stylesheets, :javascripts, :pages, :templates, :layouts, :images, :documents]
  it_behaves_like "has_and_belongs_to_many", [:forms]

  before(:each) do
    @user = FactoryGirl.create :user
    @site = FactoryGirl.create(:site, user: @user)
  end

  context 'management' do
    it 'user has many sites' do
      @user.sites.length.should == 1
    end
  end

  context "validations" do
    it "checks names for uniqueness" do
      site = @site.dup
      site.valid?.should be_false
      site.should have(1).error_on(:name)
    end

    it "checks own_domains for uniqueness" do
      @site.own_domain = 'example.com'
      @site.save

      site = @site.dup
      site.own_domain = 'example.com'
      site.valid?.should be_false
      site.should have(1).error_on(:own_domain)
    end
  end

  context "scopes" do
    it "return active sites" do
      @user.sites.active.length.should == 1
    end

    it "return public sites" do
      @site.public = true
      @site.save
      @user.sites.public.length.should == 1
    end
  end
end
