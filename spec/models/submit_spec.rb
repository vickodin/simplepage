# == Schema Information
#
# Table name: submits
#
#  id         :integer          not null, primary key
#  form_id    :integer
#  ip         :string(255)
#  ua         :string(255)
#  referer    :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Submit do
  it_behaves_like "belongs_to", [:form]
  it_behaves_like "has_many",   [:answers]
  context "validations" do
    let (:submit) { FactoryGirl.build(:submit) }

    it "valid" do
      submit.should be_valid
    end
    
  end
end
