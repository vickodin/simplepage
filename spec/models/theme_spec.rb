# == Schema Information
#
# Table name: themes
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  site_id     :integer
#  screenshot  :string(255)
#  active      :boolean          default(FALSE)
#  created_at  :datetime
#  updated_at  :datetime
#

require 'spec_helper'

describe Theme do
  it_behaves_like "belongs_to", [:site]
  let (:theme) { FactoryGirl.create(:theme) }

  context "validations" do
    it "checks names for uniqueness" do
      theme.should be_valid

      another_theme = theme.dup
      another_theme.valid?.should be_false
      another_theme.should have(1).error_on(:name)
    end
  end

  context "scopes" do
    it "no themes" do
      Theme.active.length.should == 0
    end

    it "returns active themes" do
      FactoryGirl.create(:theme, active: true)
      Theme.active.length.should == 1
    end

    it "returns active themes again" do
      FactoryGirl.create(:theme)
      FactoryGirl.create(:theme, active: true)
      Theme.active.length.should == 1
    end
  end
end
