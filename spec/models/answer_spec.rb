# == Schema Information
#
# Table name: answers
#
#  id         :integer          not null, primary key
#  submit_id  :integer
#  field_id   :integer
#  value      :text
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Answer do
  it_behaves_like "belongs_to", [:submit, :field]
  
end
