shared_examples "has_many" do |associations|
  let(:obj) { described_class.new }
  
  context "association" do
    associations.each do |association|
      it "valid has_many" do
        obj.should have_many(association).dependent(:destroy)
      end
    end
  end
end
