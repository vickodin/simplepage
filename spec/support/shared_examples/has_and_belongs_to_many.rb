shared_examples "has_and_belongs_to_many" do |associations|
  let(:obj) { described_class.new }
  
  context "association" do
    associations.each do |association|
      it "valid has_and_belongs_to_many" do
        obj.should have_and_belong_to_many(association)
      end
    end
  end
end
