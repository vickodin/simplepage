shared_examples "belongs_to" do |associations|
  let(:obj) { described_class.new }
  
  context "association" do
    associations.each do |association|
      it "valid belongs_to" do
        obj.should belong_to association
      end
    end
  end
end
