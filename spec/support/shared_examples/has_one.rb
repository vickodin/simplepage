shared_examples "has_one" do |associations|
  let(:obj) { described_class.new }
  
  context "association" do
    associations.each do |association|
      it "valid has_one" do
        obj.should have_one(association).dependent(:destroy)
      end
    end
  end
end
