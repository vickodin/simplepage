require 'spec_helper'
describe SiteCopier do
  before(:all) do
    @site = FactoryGirl.create(:site)
    
    3.times {
      @site.pages       << FactoryGirl.create(:page,        site: @site)
      @site.stylesheets << FactoryGirl.create(:stylesheet,  site: @site)
      @site.javascripts << FactoryGirl.create(:javascript,  site: @site)
      @site.layouts     << FactoryGirl.create(:layout,      site: @site)
      @site.templates   << FactoryGirl.create(:template,    site: @site)
      @site.images      << FactoryGirl.create(:image,       site: @site)
      @site.documents   << FactoryGirl.create(:document,    site: @site)

      form = FactoryGirl.create(:form, user: @site.user)
      3.times { form.fields << FactoryGirl.build(:field) }
      @site.forms << form
    }
    @theme = FactoryGirl.create(:theme, site: @site)

    copier = SiteCopier.new(@theme)
    @twin  = copier.copy('twin-name', @site.domain_id, @site.user_id)
  end

  context "same quantity" do
    it "for pages" do
      @twin.pages.length.should == @site.pages.length
    end
    it "for stylesheets" do
      @twin.stylesheets.length.should == @site.stylesheets.length
    end
    it "for javascripts" do
      @twin.javascripts.length.should == @site.javascripts.length
    end
    it "for layouts" do
      @twin.layouts.length.should == @site.layouts.length
    end
    it "for templates" do
      @twin.templates.length.should == @site.templates.length
    end
    it "for images" do
      @twin.templates.length.should == @site.templates.length
    end
    it "for forms" do
      @twin.forms.length.should == @site.forms.length
    end
    it "for documents" do
      @twin.documents.length.should == @site.documents.length
    end
  end

  context "forms structure" do
    it "same forms and fields" do
      @twin.forms.each do |twin_form|
        original_form = @site.forms.find_by_name(twin_form.name)
        original_form.fields.count.should == twin_form.fields.count

        twin_form.fields.each do |twin_field|
          original_field = original_form.fields.find_by_name!(twin_field.name)
          twin_field.kind.should == original_field.kind
        end
      end
    end
    it "one owner for forms and site" do
      @twin.forms.each do |twin_form|
        twin_form.user_id.should == @site.user_id
      end
    end
  end

  context "same content" do
    it "of pages" do
      @twin.pages.each do |p|
        p.content.should == @site.pages.find_by_name!(p.name).content
      end
    end
    it "of stylesheets" do
      @twin.stylesheets.each do |p|
        p.content.should == @site.stylesheets.find_by_name!(p.name).content
      end
    end
    it "of javascripts" do
      @twin.javascripts.each do |p|
        p.content.should == @site.javascripts.find_by_name!(p.name).content
      end
    end
    it "of layouts" do
      @twin.layouts.each do |p|
        p.content.should == @site.layouts.find_by_name!(p.name).content
      end
    end
    it "of templates" do
      @twin.templates.each do |p|
        p.content.should == @site.templates.find_by_name!(p.name).content
      end
    end
  end

end
