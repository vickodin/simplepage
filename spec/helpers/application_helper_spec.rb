require 'spec_helper'

describe ApplicationHelper do
  describe "stats values" do
    it "returns values < 10" do
      expect(helper.stats(7)).to eq 7
    end

    it "returns values in 10..1000" do
      expect(helper.stats(25)).to eq "20+"
    end

    it "returns values in 1000..10 000" do
      expect(helper.stats(1930)).to eq "1000+"
      expect(helper.stats(5930)).to eq "5000+"
      expect(helper.stats(4000)).to eq "4000+"
    end

    it "returns values in 10 000..10 0000" do
      expect(helper.stats(14001)).to eq "10000+"
    end

    it "returns values in 100 000..1 000 000" do
      expect(helper.stats(500000)).to eq "500000+"
      expect(helper.stats(700000)).to eq "700000+"
      expect(helper.stats(999999)).to eq "900000+"
    end
    
    it "returns values > 1 000 000" do
      expect(helper.stats(12000001)).to eq "12000000+"
      expect(helper.stats(12003001)).to eq "12000000+"
      expect(helper.stats(12100001)).to eq "12000000+"
      expect(helper.stats(19999999)).to eq "19000000+"
      expect(helper.stats(20000001)).to eq "20000000+"
    end

  end
end
