# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :document do
    site
    name { generate(:next_name) }
  end
end
