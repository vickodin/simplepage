# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :form do
    name { generate(:next_name) }
    user
  end
end
