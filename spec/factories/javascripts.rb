# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :javascript do
    name { generate(:next_name) }
    content 'sample js content'
    site
  end
end
