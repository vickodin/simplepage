# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :site do
    name { generate(:subdomain_name) }
    user
    domain
  end
end
