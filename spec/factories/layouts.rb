# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :layout do
    name { generate(:next_name) }
    content 'sample layout content'
    site
  end
end
