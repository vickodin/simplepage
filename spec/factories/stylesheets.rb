# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :stylesheet do
    name { generate(:next_name) }
    content 'sample stylesheet content'
    site
  end
end
