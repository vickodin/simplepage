# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :page do
    name { generate(:next_name) }
    kind 'html'
    content 'sample'
    site
  end
end
