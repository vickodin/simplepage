# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :traffic do
    site nil
    hosts 1
    hits 1
    visitors 1
  end
end
