# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :submit do
  	form
  	ip "127.0.0.1"
  	ua "bot"
  	referer "http://localhost"
  end
end
