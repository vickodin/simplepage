# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    email
    password 'password'
    password_confirmation 'password'
    activation_state 'active'
  end
end
