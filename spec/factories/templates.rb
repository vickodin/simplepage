# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :template do
    name { generate(:next_name) }
    content 'sample template content'
    site
  end
end
