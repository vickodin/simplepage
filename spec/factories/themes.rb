# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :theme do
    name { generate(:next_name) }
    description "MyText"
    site
    screenshot "MyString"
  end
end
