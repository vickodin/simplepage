# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  sequence :email do |n|
    "user#{n}@example.com"
  end

  sequence :subdomain_name do |n|
    "subdomain#{n}"
  end

  sequence :next_name do |n|
    "name#{n}"
  end

end
