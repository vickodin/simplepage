NAME=`date +'%u'`
NEW_NAME="simplepage-public-""$NAME"".tar.bz2"

cd /home/devel/simplepage.biz/uploads

tar cjf /home/devel/simplepage.biz/backup/public.tar.bz2 uploads

cd /home/devel/simplepage.biz/backup

s3cmd put public.tar.bz2 s3://s8/"$NEW_NAME"