NAME=`date +'%d%H'`
NEW_NAME="simplepage.mysql-""$NAME"".tar.bz2"

cd /home/devel/simplepage.biz/backup

mysqldump -usimplepage -psimplepage simplepage > simplepage.mysql

tar cjf simplepage.mysql.tar.bz2 simplepage.mysql

s3cmd put simplepage.mysql.tar.bz2 s3://s8/"$NEW_NAME"