class CreateTemplates < ActiveRecord::Migration
  def change
    create_table :templates do |t|
      t.string :name
      t.text :content
      t.references :site

      t.timestamps
    end
    add_index :templates, :site_id
    add_index :templates, [:site_id, :name], :unique => true
  end
end
