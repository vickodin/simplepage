class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :name
      t.string :photo
      t.references :site

      t.timestamps
    end
    add_index :images, :site_id
  end
end
