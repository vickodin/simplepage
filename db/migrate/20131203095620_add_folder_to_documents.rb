class AddFolderToDocuments < ActiveRecord::Migration
  def change
    add_column :documents, :folder, :boolean, default: false, null: false
  end
end
