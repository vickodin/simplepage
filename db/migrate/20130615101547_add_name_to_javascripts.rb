class AddNameToJavascripts < ActiveRecord::Migration
  def change
    add_column :javascripts, :name, :string, :null => false, :default => ''
    add_index :javascripts, [:site_id, :name], :unique => true
  end
end
