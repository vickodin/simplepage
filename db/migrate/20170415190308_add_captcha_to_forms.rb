class AddCaptchaToForms < ActiveRecord::Migration
  def change
    add_column :forms, :captcha, :boolean, default: false, null: false
  end
end
