class AddScreenshotToPages < ActiveRecord::Migration
  def change
    add_column :pages, :screenshot, :string
  end
end
