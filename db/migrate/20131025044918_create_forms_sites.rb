class CreateFormsSites < ActiveRecord::Migration
  def up
    create_table :forms_sites, :id => false do |t|
      t.references :form
      t.references :site
    end
    add_index :forms_sites, [:form_id, :site_id], unique: true
  end

  def down
    drop_table :forms_sites
  end
end
