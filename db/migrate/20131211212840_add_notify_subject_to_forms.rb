class AddNotifySubjectToForms < ActiveRecord::Migration
  def change
    add_column :forms, :notify_subject, :string
  end
end
