class RemoveNameIndexFromTemplates < ActiveRecord::Migration

  def up
    remove_index :templates, [:site_id, :name]
  end

  def down
    add_index :templates, [:site_id, :name], unique: true
  end

end
