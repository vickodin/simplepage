class AddSiteIdToStylesheets < ActiveRecord::Migration
  def change
    add_column :stylesheets, :site_id, :integer
    add_index :stylesheets, :site_id
  end
end
