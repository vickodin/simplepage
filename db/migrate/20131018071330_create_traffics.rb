class CreateTraffics < ActiveRecord::Migration
  def change
    create_table :traffics do |t|
      t.references :site
      t.integer :hosts
      t.integer :hits
      t.integer :visitors

      t.timestamps
    end
    add_index :traffics, :site_id
  end
end
