class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.references :submit
      t.references :field
      t.text :value

      t.timestamps
    end
    add_index :answers, :submit_id
    add_index :answers, :field_id
  end
end
