class AddKindToFields < ActiveRecord::Migration
  def change
    add_column :fields, :kind, :string
  end
end
