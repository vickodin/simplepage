class AddModifiedAtToSites < ActiveRecord::Migration
  def change
    add_column :sites, :modified_at, :datetime, null: false, default: Time.current
  end
end
