class AddNestingToTemplates < ActiveRecord::Migration
  def change
    add_column :templates, :folder, :boolean, default: false, null: false
    add_column :templates, :parent_id, :integer
  end
end
