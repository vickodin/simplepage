class AddOwnDomainToSites < ActiveRecord::Migration
  def change
    add_column :sites, :own_domain, :string, :default => '', :null => false
    add_index :sites, :own_domain
  end
end
