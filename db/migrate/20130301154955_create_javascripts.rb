class CreateJavascripts < ActiveRecord::Migration
  def change
    create_table :javascripts do |t|
      t.text :content

      t.timestamps
    end
  end
end
