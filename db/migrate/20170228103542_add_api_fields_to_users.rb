class AddApiFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :api_key, :string
    add_column :users, :api_pass, :string
  end
end
