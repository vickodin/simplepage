class AddSitemapToPages < ActiveRecord::Migration
  def change
    add_column :pages, :sitemap, :boolean, default: false, null: false
  end
end
