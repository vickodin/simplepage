class CreateFields < ActiveRecord::Migration
  def change
    create_table :fields do |t|
      t.references :form
      t.string :name, null: false

      t.timestamps
    end
    add_index :fields, :form_id
  end
end
