class RemovePageIdFromCacher < ActiveRecord::Migration
  def up
    remove_column :cachers, :page_id
  end

  def down
    add_column :cachers, :page_id, :integer
  end
end
