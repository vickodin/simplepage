class AddKindToPages < ActiveRecord::Migration
  def change
    add_column :pages, :kind, :string, :default => 'html', :null => false
    add_index :pages, [:site_id, :kind, :name], :unique => true
  end
end
