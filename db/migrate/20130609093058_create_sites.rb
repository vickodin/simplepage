class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :name
      t.references :user
      t.references :domain

      t.timestamps
    end
    add_index :sites, :user_id
    add_index :sites, :domain_id
  end
end
