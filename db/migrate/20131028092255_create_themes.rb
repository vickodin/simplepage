class CreateThemes < ActiveRecord::Migration
  def change
    create_table :themes do |t|
      t.string :name
      t.text :description
      t.references :site, index: true
      t.string :screenshot
      t.boolean :active, default: false
      
      t.timestamps
    end
  end
end
