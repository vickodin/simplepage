class AddSiteIdToJavascripts < ActiveRecord::Migration
  def change
    add_column :javascripts, :site_id, :integer
    add_index :javascripts, :site_id
  end
end
