class CreateLayouts < ActiveRecord::Migration
  def change
    create_table :layouts do |t|
      t.string :name, :null => false, :default => ''
      t.text :content
      t.references :site

      t.timestamps
    end
    add_index :layouts, :site_id
    add_index :layouts, [:site_id, :name], :unique => true
  end
end
