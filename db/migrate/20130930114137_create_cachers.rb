class CreateCachers < ActiveRecord::Migration
  def change
    create_table :cachers do |t|
      t.references :page
      t.text :content

      t.timestamps
    end
    add_index :cachers, :page_id
  end
end
