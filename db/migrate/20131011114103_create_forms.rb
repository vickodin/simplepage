class CreateForms < ActiveRecord::Migration
  def change
    create_table :forms do |t|
      t.references :user
      t.string :name
      t.string :uuid, null: false

      t.timestamps
    end
    add_index :forms, :user_id
    add_index :forms, :uuid
  end
end
