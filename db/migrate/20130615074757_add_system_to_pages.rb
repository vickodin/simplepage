class AddSystemToPages < ActiveRecord::Migration
  def change
    add_column :pages, :system, :boolean, :null => false, :default => false
  end
end
