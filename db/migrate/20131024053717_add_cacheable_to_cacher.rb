class AddCacheableToCacher < ActiveRecord::Migration
  def change
    Cacher.delete_all
    add_column :cachers, :cacheable_type, :string, null: false, default: 'Page'
    add_column :cachers, :cacheable_id, :integer
    add_index :cachers, [:cacheable_type, :cacheable_id]
  end
end
