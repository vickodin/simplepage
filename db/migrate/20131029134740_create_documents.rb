class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.references :site, index: true
      t.string :name
      t.string :attachment

      t.timestamps
    end
  end
end
