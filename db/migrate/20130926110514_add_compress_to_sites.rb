class AddCompressToSites < ActiveRecord::Migration
  def change
    add_column :sites, :compress, :boolean, default: true
  end
end
