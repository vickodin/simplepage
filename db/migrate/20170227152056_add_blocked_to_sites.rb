class AddBlockedToSites < ActiveRecord::Migration
  def change
    add_column :sites, :blocked, :boolean, default: false, null: false
  end
end
