class AddNameToStylesheets < ActiveRecord::Migration
  def change
    add_column :stylesheets, :name, :string, :null => false, :default => ''
    add_index :stylesheets, [:site_id, :name], :unique => true
  end
end
