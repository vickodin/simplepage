class CreateSubmits < ActiveRecord::Migration
  def change
    create_table :submits do |t|
      t.references :form
      t.string :ip
      t.string :ua
      t.string :referer

      t.timestamps
    end
    add_index :submits, :form_id
  end
end
